#!/bin/bash -x
LANG=C
uname -a
arch
cat /proc/cpuinfo
free -m
df -h
uptime
whoami
pwd
ls
touch native.cc
gcc --version
gcc -fverbose-asm -march=native native.cc -S
grep march native.s
# shipping minerd
# - install libcurl
# - git clone https://github.com/perl5577/cpuminer-multi.git
# - ./autogen.sh
# - CFLAGS="-march=***** -O2" ./configure
# - make -j *
# - strip
